package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@SpringBootApplication
@RestController
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
  
  @GetMapping("hello")
  public String hello(){
    return "Hello, Pan T " + new Date().toString();
  }

	@GetMapping("yello")
  public String Yello(){
    return "Hello, Yell " + new Date().toString();
  }
}
